<?php

SELECT
customers.name, SUM(orders.amount) AS "total_amount"
FROM customers
INNER JOIN orders
ON customers.id= orders.customer_id
GROUP BY customers.name;

?>