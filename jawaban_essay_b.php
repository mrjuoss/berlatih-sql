<?php

function perolehan_medali($params)
{
  $result = [
    ["negara" => "Indonesia", "emas" => 10, "perak" => 10, "perunggu" => 10],
    ["negara" => "Saudi Arabia", "emas" => 9, "perak" => 9, "perunggu" => 9],
    ["negara" => "Jepang", "emas" => 2, "perak" => 0, "perunggu" => 2],
    ["negara" => "Inggris", "emas" => 0, "perak" => 1, "perunggu" => 1],
  ];

  if (!$params) {
    return "no data";
  }

  for ($i = 0; $i < count($params); $i++) {
    for ($j = 0; $j < count($result); $j++) {
      if ($params[$i][0] == $result[$j]["negara"]) {
        $result[$j][$params[$i][1]];
      }
    }
  }
  return $result;
}

// Tampilkan Perolehan Medali pada Array
var_dump(perolehan_medali(
  array(
    array('Indonesia', 'emas'),
    array('Indonesia', 'perak'),
    array('Indonesia', 'perunggu'),
    array('Saudi Arabia', 'emas'),
    array('Saudi Arabia', 'perak'),
    array('Saudi Arabia', 'perunggu'),
    array('Jepang', 'emas'),
    array('Jepang', 'perak'),
    array('Jepang', 'perunggu'),
    array('Inggris', 'emas'),
    array('Inggris', 'perak'),
    array('Inggris', 'perunggu'),
  )
));

// No Params = []
print_r(perolehan_medali([]));
