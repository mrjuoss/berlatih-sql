<?php

function hitung($string_data)
{
  $operator = '';
  $angka1 = '';
  $angka2 = '';
  for ($i = 0; $i < strlen($string_data); $i++) {
    if ($string_data[$i] === "+" || $string_data[$i] === "-" || $string_data[$i] === "*" || $string_data[$i] === ":" || $string_data[$i] === "%") {
      $operator = $string_data[$i];
      $angka1 = substr($string_data, 0, $i);
      $angka2 = substr($string_data, $i + 1, strlen($string_data) - 1);
      break;
    }
  }

  switch ($operator) {
    case '+':
      return $angka1 + $angka2;
      break;
    case '-':
      return $angka1 - $angka2;
      break;
    case '*':
      return $angka1 * $angka2;
      break;
    case ':':
      return $angka1 / $angka2;
      break;
    case '%':
      return $angka1 % $angka2;
      break;
    default:
      return "Periksa parameter yang anda berikan dalam fungsi";
      break;
  }
}

echo hitung("102*2");
echo "<br />";
echo hitung("2+3");
echo "<br />";
echo hitung("100:25");
echo "<br />";
echo hitung("10%2");
echo "<br />";
echo hitung("99-2");
echo "<br />";
echo hitung("99 ditambah 2");
