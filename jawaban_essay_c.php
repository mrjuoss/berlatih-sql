<?php

// Query CREATE TABLE customers
CREATE TABLE IF NOT EXISTS customers (
  id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(255),
  email VARCHAR(255),
  password VARCHAR(255),
  PRIMARY KEY (id)
);

// QUERY CREATE TABLE orders
CREATE TABLE IF NOT EXISTS
orders (
  id INT NOT NULL AUTO_INCREMENT,
  amount VARCHAR(255),
  customer_id INT,
  PRIMARY KEY (id),
  CONSTRAINT customer_id
	FOREIGN KEY (customer_id)
	REFERENCES customers (id)
);

?>